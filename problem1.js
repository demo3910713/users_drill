
//Q1 Find all users who are interested in playing video games.


function findUsersByInterest(usersData, interest) {
    if(typeof usersData == "Object"){
    const usersWithInterest = [];

    Object.keys(usersData).forEach(userName => {
        const interests = usersData[userName].interests;
        if ( interests.some(int => int.includes(interest))) {
            usersWithInterest.push(userName);
        }
    });

    return usersWithInterest;
   }else{
    return "Data Insufficient"
  }
} 

module.exports=findUsersByInterest