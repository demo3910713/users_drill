const users = require("./1-users.cjs");

function findUsersWithMasters(usersData) {
    if(typeof usersData === "Object"){
    return Object.keys(usersData).filter(userName => {
        return usersData[userName].qualification.toLowerCase().includes("masters");
    });
}else{
    return "Data Insufficient"
  }
}
module.exports=findUsersWithMasters
